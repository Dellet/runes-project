const production = true; // process.env.NODE_ENV !== 'local';
const development = !production;
const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  productionSourceMap: development,
  publicPath: './',
  outputDir: path.join(__dirname, 'dist'),
  configureWebpack: {
    devtool: production ? false : '#eval-source-map',
    plugins: development ? [] : [
      // new HtmlWebpackPlugin(),
      new PrerenderSPAPlugin({
        // Required - The path to the webpack-outputted app to prerender.
        staticDir: path.join(__dirname, 'dist'),
        // Required - Routes to render.
        routes: [
          '/',
          '/menu',
          '/futark',
          '/iceland',
          '/meadows',
          '/armanic',
        ],
        /*postProcess: function (context) {
          const titles = {
            '/': 'Рунодел - ресурс рунной деятельности',
            '/menu': 'Рунное меню | Рунодел',
            '/futark': 'Руны Футарк | Рунодел',
            '/iceland': 'Руны Исландии | Рунодел',
            '/meadows': 'Руны Утарк | Рунодел',
            '/armanic': 'Руны Арманен | Рунодел',
          };
          return context.html.replace(
            /<title>[^<]*<\/title>/i,
            '<title>' + titles[context.route] + '</title>'
          )
        }*/
      })
    ],
    module: {}
  }
};
