import Api from './api-service';

export default {
  getCategories() {
    return Api().get('/categories');
  }
}
