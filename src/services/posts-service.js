import Api from './api-service';

export default {
  name: 'PostsService',
  getPosts() {
    return Api().get('/posts');
  },
  getPost(id) {
    return Api().get('/posts/' + id);
  }
}
