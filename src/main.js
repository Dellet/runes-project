import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import VueRouter from 'vue-router';
import VueMeta from 'vue-meta';
import Futark from '@/components/Futark';
import Iceland from '@/components/Iceland';
import Navigation from "@/components/Navigation";
import Hexgrid from "@/components/hex-grid/HexGrid";
import Armanic from "@/components/Armanic";
import Reich from "@/components/Reich";
import Vue2TouchEvents from 'vue2-touch-events';
import VueRx from 'vue-rx';
import Meadows from "@/components/Meadows";
import VideoBg from 'vue-videobg'
// import Home from "@/components/Home";

import SVGInjectorVue from 'svginjector-vue'

Vue.use(SVGInjectorVue)

const routes = [
    // {path: '/', component: Home,  meta: { title: 'Рунодел - ресурс рунной деятельности' }},
    {path: '/', component: Navigation,  meta: { title: 'Рунодел - рунное меню' }},
    {path: '/futark', component: Futark, props: {defaultRune: 'Fuhu'},  meta: { title: 'Рунодел - Футарк' }},
    {path: '/iceland', component: Iceland, props: {defaultRune: 'Plastur'}},
    {path: '/meadows', component: Meadows, props: {defaultRune: 'Ur'}},
    {path: '/armanic', component: Armanic, props: {defaultRune: 'Ur'}},
    {path: '/reich', component: Reich, props: {defaultRune: 'Sonnenrad'}},
    {path: '/hexgrid', component: Hexgrid, props: {}},
];

const router = new VueRouter({
    base: "/",
    routes,
    //mode: 'history'
});

Vue.component('video-bg', VideoBg);
Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(Buefy);
Vue.use(Vue2TouchEvents);
Vue.use(VueRx);
Vue.use(VueMeta, { refreshOnceOnNavigation: true });
new Vue({
    render: h => h(App),
    router
}).$mount('#app');
